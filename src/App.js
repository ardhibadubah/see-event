import BootstrapComponents from './components/BootstrapComponents';

function App() {
  return (
    <div>
      <BootstrapComponents />
    </div>
  );
}

export default App;
